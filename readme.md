# My personal setup after a clean Mac OS install.
This is my checklist for clean installing my Mac.

## First things first
- [ ] Update to the most recent OS version (now Mojave)
- [ ] Backup the Sequel Pro databases
- [ ] Pause the Dropbox sync on all open devices
- [ ] Make an external copy of...
	- [ ] The complete Dropbox folder to avoid waiting to download > 1TB after clean instal
	- [ ] The Desktop, Documents, Movies & Pictures folder
- [ ] Update brewfile:
	- [ ] Dump softwares list to `Brewfile` file with `brew bundle dump` and compare with the 'old' one
	- [ ] [List all current Brew files by App store](https://github.com/mas-cli/mas) with `mas list`, add them to 'old' brewfile
- [ ] [Format and Install new OS](https://support.apple.com/nl-nl/HT204904)

## After clean install
- [ ] Install X-code from App Store
	- [ ] Open it and accept license agreement
- [ ] Install softwares
	- [ ] Install [Homebrew](https://brew.sh) with `/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"`
	- [ ] Install [mas-cli](https://github.com/mas-cli/mas) with `brew install mas`
   - [ ] Copy `Brewfile` to home folder then run `brew bundle`
- [ ] Setup Teamviewer Initial Setup
- [ ] Setup Moom [genaral](img/moom-general.png) & [mouse](img/moom-mouse.png) settings
- [ ] Setup Dropbox en replace content folder from external copy
- [ ] Replace Desktop files en folders from external copy
- [ ] Restore System Preferences
	- [ ] Desktop & Screen Saver, use [Apple TV Aerial Screen Saver for Mac](https://github.com/JohnCoates/Aerial)
	- [ ] Dock
		- [ ] Use [Dock settings](img/dock-settings.png)
		- [ ] Use [Dock view](/img/dock-view.png)
	- [ ] [Mission control settings](/img/mission-control-settings.png)
	- [ ] [Language & Region settings](/img/language-region.png)
	- [ ] iCloud (sync everything except Photos on work environment)
	- [ ] Mouse (check everything en set tracking speed tot 7th)
- [ ] Show hidden files in Finder (shortcut `⌘ CMD + ⇧SHIFT + .`)
- [ ] Atom environment
	- [ ] Themes
		- [ ] UI theme: Atom Dark
		- [ ] Syntax theme: Monokai
	- [ ] Community Packages
		- [ ] atom-beautify
			- Config SCSS settings
				- Default Beautifier to `CSScomb`
				- Check Beautify On Save
				- Set custom config file to `/Users/patrick/Dropbox/Apps (sync)/Atom/.csscomb.json`
		- [ ] color-picker
		- [ ] emmet
		- [ ] platformio-ide-terminal
- [ ] Setup 1Password dropbox sync
- [ ] Setup Alfred
	- [ ] Hotkey `⌘ Space` (set Spotlight shortcut to `⌥ Space`)
	- [ ] Activate powerpack
	- [ ] Sync 'Advanced » Syncing » Set prefereces folder' to Dropbox
- Setup Creative Cloud
	- [ ] Install InDesign, Photoshop, Illustrator & XD
- Setup Airmail
	- [ ] Setup Spamsieve
	 - [ ] Hide Spamsieve icon [how to](https://c-command.com/spamsieve/help/how-can-i-hide-spamsiev)
	- [ ]Config Spamsieve in Airmail
- Setup Safari
	- [ ] Config developer mode
	- [ ] Set homepage to `https://patrickverwoerd.nl/homepage/`
	- [ ] Show Favourites Bar
- Setup iTerm2
	- [ ] General » Load preferences from `/Users/patrick/Dropbox/Apps (sync)`
	- [ ] General » Uncheck 'Confirm "Quit iTerm2 (⌘Q)" if windows open'
	- [ ] Profiles » Set working directory to `/Users/patrick/Dropbox/Websites`
- [Setup Bitbucket SSH key](https://confluence.atlassian.com/bitbucket/set-up-an-ssh-key-728138079.html)
	- [ ] From the terminal, enter `ssh-keygen` & follow the steps
	- [ ] Enter `ssh-add -K ~/.ssh/id_rsa`
	- [ ] Create config file in `the ~/.ssh/config` and add
	```
	Host *
		UseKeychain yes
	```
	- [ ] Copy SSH key from public key file `pbcopy < ~/.ssh/id_rsa.pub`
	- [ ] Add new key in [Bitbucket account settings](https://bitbucket.org/account/user/PatrickVerwoerd/ssh-keys/) (don't edit, add new)
- [Setup Github](https://help.github.com/articles/adding-a-new-ssh-key-to-your-github-account/)
	- [ ] Copy SSH key from public key file `pbcopy < ~/.ssh/id_rsa.pub`
	- [ ] Add new key in [Github account settings](https://github.com/settings/keys) (don't edit, add new)
- Setup Laravel
	- [ ] Copy SSH key from public key file `pbcopy < ~/.ssh/id_rsa.pub`
	- [ ] Add new key in [Laravel Profile settings](https://forge.laravel.com/user/profile#/keys)
- Setup Tower app service accounts
- Install [Laravel Valet](https://laravel.com/docs/master/valet#installation)
	- [ ] Install Valet with composer `composer global require laravel/valet`
	- [ ] Run `valet install`
		- Set `~/.composer/vendor/bin` in the systems "PATH":
			- [ ] [Create an Bash Profile file](https://threenine.co.uk/add-laravel-to-path-on-mac-osx/) by typing `subl ~/.bash_profile` and add `export PATH="$PATH:$HOME/.composer/vendor/bin"` in it.
	- [ ] Run `brew install mysql@5.7`
		-	[ ] Run `brew services start mysql@5.7`
	- [ ] [Serve sites from 'website' directory (/Dropbox/Websites)](https://laravel.com/docs/master/valet#serving-sites) by running `valet park`
- [ ] [Install Gulp](https://gulpjs.com/docs/en/getting-started/quick-start)
	 - Install NPM Natives `npm install natives`
- [ ] Enable PHP short tags, set `short_open_tag = On` in `/usr/local/etc/php/*/php.ini` line ±192
